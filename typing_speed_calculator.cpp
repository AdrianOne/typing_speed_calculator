#include <iostream>
#include <fstream>
#include <string>
#include <chrono> 

int check_text(const std::string& text, const std::string& entered_text) {
    int error_count = 0;
    if (text.length() > entered_text.length()) {
        for (std::size_t i = 0; i < entered_text.length(); i++) {
            if (text[i] != entered_text[i]) {
                error_count++;
            }
        }
        error_count += (text.length() - entered_text.length());
    } else if (text.length() < entered_text.length()) {
        std::cout << text.length() << " " << entered_text.length() << std::endl;
        for (std::size_t i = 0; i < text.length(); i++) {
            if (text[i] != entered_text[i]) {
                error_count++;
            }
        }
        error_count += (entered_text.length() - text.length()); 
    } else if (text.length() == entered_text.length()) {
        for (std::size_t i = 0; i < text.length(); i++) {
            if (text[i] != entered_text[i]) {
                error_count++;
            } 
        }
    }
    
    return error_count;
}

void start_typing_test() {
    std::string line;
    std::ifstream file("typing_test_text.txt");
    int longest_line_length = 0;
    int current_line_length = 0;
    int word_count = 0;
    int error_count = 0;
    std::string entered_text;
    std::string text;
    double words_per_minute = 0;
    
    while (std::getline(file, line)) {
        // Print line
        std::cout << line << std::endl;
        // Add line to text string
        text += line;
        // Get the longest line
        current_line_length = line.length();
        if (current_line_length > longest_line_length) {
            longest_line_length = current_line_length;
        }
        // Get word_count
        for (char c : line) {
            if (c == ' ') {
                word_count++;
            }
        }
    }
    word_count++;
    // Print seperator
    std::cout << std::string(longest_line_length, '-') << std::endl;
    std::cout << "Start typing: " << std::endl;
    
    // Get the time at the start of the typing test, start the input for the typing test,
    // and get the time at the end of the typing test
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    getline(std::cin, entered_text);
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    
    double duration_in_minutes = (double) std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000000 / 60;
    
    error_count = check_text(text, entered_text);
    
    words_per_minute = (double) word_count / duration_in_minutes;
    
    std::cout << words_per_minute << " words per minute" << std::endl;
    std::cout << error_count << (error_count == 1 ? " error" : " errors") << std::endl;
}

int main() {
    start_typing_test();
    return 0;
}
